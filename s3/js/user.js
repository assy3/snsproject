var vm = new Vue({
    el: "#app", // Vue.jsを使うタグのIDを指定
    data: {
    // Vue.jsで使う変数はここに記述する
        users: [],
        query: {
            nickname: null,
            start: null,
            end: null
        }
    },
    computed: {
    // 計算した結果を変数として利用したいときはここに記述する
        filteredUsers: function() {
            var result = this.users;
            if (this.query.nickname) {

                result = result.filter(function (target) {
                    // userのリストから取り出した一つの要素がtargetに入り、ニックネームにマッチしたら
                    // 空のリストに要素を追加していき、配列全ての操作が終えたらリストを返す
                    // nullはじく
                    if(target.nickname){
                        return target.nickname.match(vm.query.nickname);
                    }
                });
            }
            if (this.query.start) {
                result = result.filter(function (target) {
                    return target.age >= vm.query.start;
                });
            }
            if (this.query.end) {
                result = result.filter(function (target) {
                    return target.age <= vm.query.end;
                });
            }
            return result;
        }
    },
    created: function() {
    // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
        // var Id = localStorage.getItem("userId");
        fetch(url + "/users", {
            method: "GET"
        })
            .then(function(response) {
                if (response.status == 200) {
                    return response.json();
                }
                // 200番以外のレスポンスはエラーを投げる
                return response.json().then(function(json) {
                    throw new Error(json.message);
                });
            })
            .then(function(json) {
            // レスポンスが200番で返ってきたときの処理はここに記述する
            // 正常処理
                // console.log("取得できました");
                // console.log(json);
                // console.log(json);
                // console.log(json.users);
                vm.users = json.users;

            })
            .catch(function(err) {
            // レスポンスがエラーで返ってきたときの処理はここに記述する
                console.log(err);
            });
    },
    methods: {
    // Vue.jsで使う関数はここで記述する
    },
});
