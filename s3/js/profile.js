// ビューオブジェクト生成
var vm = new Vue({
    el: "#app", // Vue.jsを使うタグのIDを指定
    data: {
    // Vue.jsで使う変数はここに記述する

        user: {
            userId: null,
            password: null,
            nickname: null,
            age: null
        }
    },
    computed: {
    // 計算した結果を変数として利用したいときはここに記述する

    },
    created: function() {
    // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
        // var Id = localStorage.getItem("userId");
        fetch(url + "/user" +
            "?userId=" + localStorage.getItem("userId"), {
            method: "GET"
        })
            .then(function(response) {
                if (response.status == 200) {
                    return response.json();
                }
                // 200番以外のレスポンスはエラーを投げる
                return response.json().then(function(json) {
                    throw new Error(json.message);
                });
            })
            .then(function(json) {
            // レスポンスが200番で返ってきたときの処理はここに記述する
            // 正常処理
                console.log("取得できました");
                console.log(json);
                vm.user.userId = json.userId;
                vm.user.nickname = json.nickname;
                vm.user.age = json.age;
            })
            .catch(function(err) {
            // レスポンスがエラーで返ってきたときの処理はここに記述する
                console.log(err)
            });
    },
    methods: {
    // Vue.jsで使う関数はここで記述する

        submit: function () {
            fetch(url + "/user", {
                method: "PUT",
                headers: new Headers({
                    "Authorization": localStorage.getItem('token')
                }),
                body: JSON.stringify({
                    "userId":vm.user.userId,
                    "password": vm.user.password,
                    "nickname": vm.user.nickname,
                    "age": Number(vm.user.age)
                })
            })
                .then(function(response) {
                    if (response.status == 200) {
                        return response.json();
                    }
                    // 200番以外のレスポンスはエラーを投げる
                    return response.json().then(function(json) {
                        throw new Error(json.message);
                    });
                })
                .then(function(json) {
                    // レスポンスが200番で返ってきたときの処理はここに記述する
                    var content = JSON.stringify(json, null, 2);
                    //var content = JSON.stringify(json);
                    console.log(content);
                    localStorage.setItem('token', json.token);
                    localStorage.setItem('userId', vm.user.userId);
                    //トップページへ遷
                    location.href = "./index.html";

                })
                .catch(function(err) {
                    // レスポンスがエラーで返ってきたときの処理はここに記述する
                });

        },
        deleteUser: function () {
            fetch(url + "/user", {
                method: "DELETE",
                headers: new Headers({
                    "Authorization": localStorage.getItem('token')
                }),
                body: JSON.stringify({
                    userId: vm.user.userId,
                    password: vm.user.password
                })
            })
                .then(function (response) {
                    console.log("satus 200 ok");
                    if (response.status == 200) {
                        return response.json();
                    }
                    // 200番以外のレスポンスはエラーを投げる
                    return response.json().then(function (json) {
                        throw new Error(json.message);
                    });
                })
                .then(function (json) {
                    console.log("json ok");
                    // レスポンスが200番で返ってきたときの処理はここに記述する
                    location.href = "./login.html";
                })
                .catch(function (err) {
                    console.log(err);
                    // レスポンスがエラーで返ってきたときの処理はここに記述する
                });
        }
    },
});
